## Example API app using Express and MongoDB
### How to run

Install dependencies
```
npm install
```
Run the container with MongoDB and the server.
```
npm start
```
Note: you need to have a docker container named 'test-mongo' with MongoDB. The app uses a database named 'mern-api-example'
### Endpoints

- GET /products (optional query parameters: fieldname[operator]=value, sort_by[asc|desc]=fieldname)
- POST /products - provide the document to be added in json body, field "name" is required
- PUT /products/:id - provide the fields to be updated in json body
- DELETE /products/:id
- GET /products/report
- GET /products/report/all
- GET /products/report/:id