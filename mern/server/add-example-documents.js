// użyłem następujących komend w konsoli w DataGrip
// use mern-api-example
db.products.insertMany([
    {
        "name": "Laptop Dell XPS 13",
        "price": 2499.99,
        "description": "Potężny laptop z ekranem InfinityEdge.",
        "amount": 20,
        "unit": "szt"
    },
    {
        "name": "Smartfon iPhone 13 Pro",
        "price": 1299.00,
        "description": "Najnowszy model iPhone z potrójnym aparatem.",
        "amount": 15,
        "unit": "szt"
    },
    {
        "name": "Telewizor Samsung QLED 65 cali",
        "price": 3499.99,
        "description": "Telewizor 4K QLED z technologią Quantum Dot.",
        "amount": 10,
        "unit": "szt"
    },
    {
        "name": "Kamera DSLR Canon EOS 5D Mark IV",
        "price": 2999.00,
        "description": "Profesjonalna kamera cyfrowa z matrycą pełnoklatkową.",
        "amount": 12,
        "unit": "szt"
    },
    {
        "name": "Zmywarka Bosch Serie 6",
        "price": 899.99,
        "description": "Zmywarka ze specjalnym programem do naczyń delikatnych.",
        "amount": 25,
        "unit": "szt"
    },
    {
        "name": "Głośniki bezprzewodowe Sonos One",
        "price": 199.00,
        "description": "Inteligentne głośniki z obsługą głosową.",
        "amount": 30,
        "unit": "szt"
    },
    {
        "name": "Słuchawki Bose QuietComfort 35 II",
        "price": 349.95,
        "description": "Bezprzewodowe słuchawki z redukcją hałasu.",
        "amount": 18,
        "unit": "szt"
    },
    {
        "name": "Konsola do gier PlayStation 5",
        "price": 499.99,
        "description": "Najnowsza konsola do gier z superszybkim SSD.",
        "amount": 8,
        "unit": "szt"
    },
    {
        "name": "Monitor LG UltraGear 27GN950-B",
        "price": 1499.99,
        "description": "Monitor gamingowy z rozdzielczością 4K.",
        "amount": 22,
        "unit": "szt"
    },
    {
        "name": "Lodówka Samsung Family Hub",
        "price": 3299.00,
        "description": "Lodówka z wbudowanym ekranem dotykowym.",
        "amount": 14,
        "unit": "szt"
    },
    {
        "name": "Smartwatch Apple Watch Series 7",
        "price": 399.00,
        "description": "Najnowszy smartwatch z większym ekranem Retina.",
        "amount": 17,
        "unit": "szt"
    },
    {
        "name": "Rowerek treningowy Schwinn IC4",
        "price": 799.00,
        "description": "Rowerek treningowy do intensywnego cardio.",
        "amount": 20,
        "unit": "szt"
    },
    {
        "name": "Ekspres do kawy Breville Barista Express",
        "price": 699.99,
        "description": "Profesjonalny ekspres do kawy z młynkiem.",
        "amount": 16,
        "unit": "szt"
    },
    {
        "name": "Odkurzacz robot Xiaomi Roborock S7",
        "price": 599.00,
        "description": "Odkurzacz z funkcją mopowania i nawigacją laserową.",
        "amount": 13,
        "unit": "szt"
    },
    {
        "name": "Mikrofalówka Panasonic NN-SN966S",
        "price": 169.99,
        "description": "Mikrofalówka z inwerterowym grillem.",
        "amount": 28,
        "unit": "szt"
    },
    {
        "name": "Aparat do kawy AeroPress",
        "price": 29.95,
        "description": "Prosty i skuteczny sposób na kawę.",
        "amount": 40,
        "unit": "szt"
    },
    {
        "name": "Hulajnoga elektryczna Segway Ninebot MAX",
        "price": 799.00,
        "description": "Hulajnoga z dużym zasięgiem i wygodnym siedziskiem.",
        "amount": 10,
        "unit": "szt"
    },
    {
        "name": "Żelazko parowe Philips PerfectCare Elite",
        "price": 149.99,
        "description": "Żelazko z automatyczną regulacją temperatury.",
        "amount": 18,
        "unit": "szt"
    },
    {
        "name": "Deska do prasowania Brabantia",
        "price": 79.95,
        "description": "Stabilna deska do prasowania z regulacją wysokości.",
        "amount": 25,
        "unit": "szt"
    },
    {
        "name": "Blender Vitamix E310",
        "price": 349.95,
        "description": "Mocny blender do smoothie i zup kremowych.",
        "amount": 15,
        "unit": "szt"
    },
    {
        "name": "Lampa biurkowa LED TaoTronics",
        "price": 39.99,
        "description": "Lampa z regulacją jasności i koloru światła.",
        "amount": 30,
        "unit": "szt"
    },
    {
        "name": "Dron DJI Mavic Air 2",
        "price": 799.00,
        "description": "Kompaktowy dron z kamerą 4K i inteligentnymi funkcjami lotu.",
        "amount": 12,
        "unit": "szt"
    },
    {
        "name": "Kurtka outdoorowa The North Face",
        "price": 199.99,
        "description": "Wodoodporna kurtka na wszystkie warunki atmosferyczne.",
        "amount": 20,
        "unit": "szt"
    },
    {
        "name": "Stół do ping ponga Cornilleau 500M",
        "price": 1099.00,
        "description": "Profesjonalny stół do ping ponga z regulacją wysokości.",
        "amount": 8,
        "unit": "szt"
    },
    {
        "name": "Nawilżacz powietrza Dyson AM10",
        "price": 499.00,
        "description": "Nawilżacz powietrza z funkcją oczyszczania.",
        "amount": 15,
        "unit": "szt"
    },
    {
        "name": "Grill gazowy Weber Spirit II E-310",
        "price": 499.00,
        "description": "Grill gazowy z trzema palnikami i termometrem.",
        "amount": 10,
        "unit": "szt"
    },
    {
        "name": "Łóżko kontynentalne Sleep Number 360",
        "price": 2499.99,
        "description": "Łóżko z regulacją twardości i ustawieniem poduszek.",
        "amount": 12,
        "unit": "szt"
    },
    {
        "name": "Kosiarka automatyczna Husqvarna Automower 450X",
        "price": 3499.00,
        "description": "Kosiarka robotyczna z nawigacją GPS.",
        "amount": 7,
        "unit": "szt"
    },
    {
        "name": "Kettlebell KETTLEGRYP",
        "price": 34.95,
        "description": "Przenośny obciążnik do treningu siłowego.",
        "amount": 25,
        "unit": "szt"
    },
    {
        "name": "Wózek dziecięcy Bugaboo Cameleon3",
        "price": 899.00,
        "description": "Wózek wielofunkcyjny z możliwością montażu gondoli i fotelika samochodowego.",
        "amount": 15,
        "unit": "szt"
    }
]);