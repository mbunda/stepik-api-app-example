const express = require("express");
const recordRoutes = express.Router();
const dbo = require("../db/conn");
const ObjectId = require("mongodb").ObjectId;
const qs = require('qs'); // query filter sort parser

dbo.connectToServer(function (err) {
    if (err) {
        console.error("Error connecting to database:", err);
    } else {
        console.log("Connected to MongoDB.");
    }
});

// attach products collection to req object so code is not repeated
recordRoutes.use(async function(req, res, next) {
    try {
        let db_connect = dbo.getDb();
        let products = db_connect.collection("products");
        req.products = products; 
        next();
    } catch (err) {
        console.error("Error attaching products collection:", err);
        res.status(500).send("Internal server error");
    }
});

recordRoutes.route("/products").get(async function(req, res) {
    try {
        const products = req.products;
        const parsedQuery = qs.parse(req.query)
        const [filter, sort] = resolveFilterSort(parsedQuery)
        console.log(parsedQuery);
        console.log(filter);
        console.log(sort);
        
        let result = await products.find(filter).sort(sort).toArray();
        res.json(result);
    } catch (err) {
        console.error(err);
        res.status(500).send("Internal server error");
    }
    
});

function resolveFilterSort(parsedQuery) {
    const filter = {}
    const sort = {}
    // sorting
    if (parsedQuery.sort_by?.asc ) {
        // make array from one sort_by if its not an array
        const sort_params = Array.isArray(parsedQuery.sort_by.asc) ? parsedQuery.sort_by.asc : [parsedQuery.sort_by.asc];
        sort_params.forEach((element) => {
            sort[element] = 1;
        })
    }
    if (parsedQuery.sort_by?.desc) {
        const sort_params = Array.isArray(parsedQuery.sort_by.desc) ? parsedQuery.sort_by.desc : [parsedQuery.sort_by.desc];
        sort_params.forEach((element) => {
            sort[element] = -1;
        })
    }

    // filtering
    const notFilterKeys = ['sort_by', 'limit']
    const keys = Object.keys(parsedQuery); // name, amount, sort_by ...
    const filteredKeys = keys.filter(key => !notFilterKeys.includes(key)) // name, amount
    
    filteredKeys.forEach((key) => {
        const operator = Object.keys(parsedQuery[key])[0]; // eq, gt, lt ...
        const nestedValue = parsedQuery[key][operator]; // 'Monitor', '15', ...
        const parsedNestedValue = parseFloat(nestedValue); // NaN, 15
        const newNestedValue = isNaN(parsedNestedValue) ? nestedValue : parsedNestedValue; // 'Monitor', 15 
        const newValue = {}
        newValue[`$${operator}`] = newNestedValue; // { '$eq': 'Monitor' } , { 'gt': 15 } 
        filter[key] = newValue; // { 'name': { 'eq': 'Monitor}, 'amount': { 'gt': 15} }
    })
    return [filter, sort];
}

recordRoutes.route("/products").post(async function(req, res){
    try {
        const products = req.products;
        // check if request body has 'name' field
        if (!req.body.name) {
            res.status(400).send('Field \'name\' is required');
            return;
        }
        const newDocument = req.body;
        // check if name is unique
        const duplicate = await products.findOne({name: req.body.name}) 
        if (!duplicate) {
            let result = await products.insertOne(newDocument);
            res.status(200).send(result);
        } else {
            res.status(406).send('A document with the same \'name\' field already exists');
        }
    } catch (err) {
        console.error(err);
        res.status(500).send('Internal server error');
    }
});

recordRoutes.route("/products/:id").put(async function(req, res){
    try {
        const products = req.products;
        // filter query
        const myquery = {_id: ObjectId(req.params.id)}

        // only accept valid fields
        const { name, price, description, amount, unit } = req.body;
        // filter out undefined fields
        const newValues =  { $set: {
            ...(name && { name }),
            ...(price && { price }),
            ...(description && { description }),
            ...(amount && { amount }),
            ...(unit && { unit })
        } }

        const result = await products.updateOne(myquery, newValues)
        if (result.modifiedCount === 1) {
        res.status(200).send("Update successful")
        } else {
            res.status(400).send("Update unsuccesful")
        }
    } catch (err) {
        console.error(err);
        res.status(500).send("Internal server error")
    }
});

recordRoutes.route("/products/:id").delete(async function (req, res) {
    try {
        const products = req.products;
        // filter query
        const myquery = { _id: ObjectId(req.params.id)};

        const result = await products.deleteOne(myquery);

        if (result.deletedCount === 1) {
            res.status(200).send("Deleted product successfully");
        } else {
            res.status(400).send("Product does not exist already");
        }
    } catch (err) {
        console.error(err);
        res.status(500).send('Internal server error');
    }
})

recordRoutes.route("/products/report").get(async function (req, res) {
    try {
        const products = req.products;
        const result = await products.aggregate([
            {
                $group: {
                    _id: null,
                    totalStock: { $sum: "$amount" },
                    totalPrice: { $sum: { $multiply: ["$price", "$amount"] } },
                    totalProducts: { $sum: 1 }
                }
            },
            {
                $project: {
                    _id: 0,
                    totalProducts: 1,
                    totalStock: 1,
                    totalPrice: 1
                }
            }
        ]).toArray();
        res.json(result);
    } catch (err) {
        console.error(err);
        res.status(500).send('Internal server error');
    }
});

recordRoutes.route("/products/report/all").get(async function (req, res) {
    try {
        const products = req.products;

        const result = await products.aggregate([
            {$match: {}},
            {$project: {
                _id: 1,
                name: 1,
                price: 1,
                amount: 1,
                totalPrice: { $multiply: ["$price", "$amount"] }
            }}
        ]).toArray();
        res.json(result);
    } catch (err) {
        console.error(err);
        res.status(500).send('Internal server error');
    }
});

recordRoutes.route("/products/report/:id").get(async function (req, res) {
    try {
        const products = req.products;
        const matchquery = {_id: ObjectId(req.params.id)}

        const result = await products.aggregate([
            {$match: matchquery},
            {$project: {
                _id: 1,
                name: 1,
                price: 1,
                amount: 1,
                totalPrice: { $multiply: ["$price", "$amount"] }
            }}
        ]).toArray();
        res.json(result);
    } catch (err) {
        console.error(err);
        res.status(500).send('Internal server error');
    }
});

module.exports = recordRoutes;