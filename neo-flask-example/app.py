init_cypher = ""
with open('init.cypher', 'r') as file:
    init_cypher = file.read()
init_queries = init_cypher.split('\n\n')
# print(init_queries) 

from flask import Flask, jsonify, request
from neo4j import GraphDatabase
from dotenv import load_dotenv
import os #provides ways to access the Operating System and allows us to read the environment variables

load_dotenv()

app = Flask(__name__)

uri = os.getenv('URI')
user = os.getenv("USERNAME")
password = os.getenv("PASSWORD")
driver = GraphDatabase.driver("bolt://localhost:7687", auth=("neo4j", "test1234"),database="neo4j")

# clear database
with driver.session() as session:
    session.run("MATCH (n) DETACH DELETE n")
for query in init_queries:
    with driver.session() as session:
        session.run(query)

# Napisz funkcję, która będzie zwracać wszystkich pracowników z bazy danych na endpoint /employees metodą GET. Użytkownik powinien mieć możliwość filtrowania i sortowania pracowników według różnych kryteriów (np. według imienia, nazwiska, stanowiska).
def get_employees(tx, args=None):
    query = ""
    if args:
        query = resolve_sort_filter(args)
    else:
        query = "MATCH (e:Employee) RETURN e"
    results = tx.run(query).data()
    employees = [{'name': result['e']['name'], 'position': result['e']['position']} for result in results]
    return employees

def resolve_sort_filter(args):
    query = "MATCH (e:Employee)"
    if 'filter' in args:
        for key, value in args['filter'].items():
            query += " WHERE e." + key + "='" + value + "'"
    query += " RETURN e"
    if 'sort' in args:                              # what about multiple sort args?
        query += " ORDER BY e." + args['sort']
    return query

@app.route('/employees', methods=['GET'])

def get_employees_route():
    args = request.args
    with driver.session() as session:
        employees = session.read_transaction(get_employees)

    response = {'employees': employees}
    return jsonify(response)


# Napisz funkcję, która będzie dodawać nowego pracownika do bazy danych na endpoint /employees metodą POST. Sprawdź czy podane imie i nazwisko są unikalne oraz czy podano wszystkie wymagane dane (np. stanowisko, departament).
def add_employee(tx, name, position, department):
    checkUnique = f"MATCH (e:Employee) WHERE e.name='{name}' RETURN e"
    resultUnique = tx.run(checkUnique).data()
    if resultUnique:
        return None
    else:
        query = f"CREATE (e:Employee {{name: '{name}', position: '{position}'}})-[:WORKS_IN]->(d:Department {{name: '{department}'}})"
        tx.run(query)
        return {'name': name, 'position': position, 'department': department}

@app.route('/employees', methods=['POST'])
def add_employee_route():
    name = request.json.get('name', None)
    position = request.json.get('position', None)
    department = request.json.get('department', None)

    if not name or not position or not department:
        response = {'error': 'Missing name, position or department'}
        return jsonify(response), 400
    
    response = {}
    with driver.session() as session:
        response = session.write_transaction(add_employee, name, position, department)
    if not response:
        response = {'error': 'Employee already exists'}
        return jsonify(response), 400
    return jsonify(response)

# Napisz funkcję, która będzie edytować istniejącego pracownika w bazie danych na endpoint /employees/:id metodą PUT. Użytkownik powinien mieć możliwość zmiany imienia, nazwiska, stanowiska oraz departamentu pracownika.
def update_employee(tx, id, name, position, department):
    query = ""
    query2 = ""
    if department:
        # connect employee to new department and delete old relationship if it exists
        query = f"MATCH (e:Employee) WHERE id(e)={id} OPTIONAL MATCH (e)-[r:WORKS_IN]->(d:Department) MATCH (d2:Department) WHERE d2.name='{department}' CREATE (e)-[:WORKS_IN]->(d2) DELETE r"
        if name or position:
            # update employee name and position if provided
            query2 = f"MATCH (e:Employee) WHERE id(e)={id} SET {'e.name='+'"'+name+'"'+', ' if name else ''} {'e.position='+'"'+position+'"' if position else ''}"
            tx.run(query2)
    else:
        # update employee name and position
        query = f"MATCH (e:Employee) WHERE id(e)={id} SET {'e.name='+'"'+name+'"'+', ' if name else ''} {'e.position='+'"'+position+'"' if position else ''}"
    tx.run(query)
        
    return {'name': name, 'position': position, 'department': department}

@app.route('/employees/<int:id>', methods=['PUT'])
def update_employee_route(id):
    name = request.json.get('name', None)
    position = request.json.get('position', None)
    department = request.json.get('department', None)

    if name or position or department:
        with driver.session() as session:
            employee = session.write_transaction(update_employee, id, name, position, department)

    if not employee:
        response = {'error': 'Employee not found'}
        return jsonify(response), 404
    else:
        response = employee
        return jsonify(response)


# Napisz funkcję, która będzie usuwać istniejącego pracownika z bazy danych na endpoint /employees/:id metodą DELETE. Jeśli pracownik jest menadżerem jakiegoś departamentu, należy ustalić nowego menadżera lub usunąć departament.
def delete_employee(tx, id):
    # check if employee is manager using optional match
    query = f"MATCH (e:Employee) WHERE id(e)={id} OPTIONAL MATCH (e)-[r:MANAGES]->(d:Department) RETURN e,d"
    result = tx.run(query).data()
    if not result:
        return None
    if tx.run(query).data()[0]['d']:
        # if employee is manager, delete manager relationship
        query = f"MATCH (e:Employee)-[r:MANAGES]->(d:Department) WHERE id(e)={id} DELETE r"
        tx.run(query)
        # if department has no other employees, delete department
        query = f"MATCH (d:Department)<-[r:WORKS_IN]-(e:Employee) WHERE id(d)={result[0]['e']['department']} RETURN e"
        deleteDepartment = tx.run(query).data()
        if not deleteDepartment:
            # if department has no other employees, delete department   
            query = f"MATCH (d:Department) WHERE id(d)={result[0]['e']['department']} DETACH DELETE d"
            tx.run(query)
        else:
            # if department has other employees, assign new manager
            query = f"MATCH (d:Department)<-[r:WORKS_IN]-(e:Employee) WHERE id(d)={result[0]['e']['department']} RETURN e"
            new_manager = tx.run(query).data()[0]['e']
            query = f"MATCH (e:Employee)-[r:WORKS_IN]->(d:Department) WHERE id(e)={new_manager['id']} CREATE (e)-[:MANAGES]->(d)"
            tx.run(query)
        
    query = f"MATCH (e:Employee) WHERE id(e)={id} DETACH DELETE e"
    tx.run(query)
    return {'id': id}

@app.route('/employees/<int:id>', methods=['DELETE'])
def delete_employee_route(id):
    with driver.session() as session:
        employee = session.write_transaction(delete_employee, id)

    if not employee:
        response = {'error': 'Employee not found'}
        return jsonify(response), 404
    else:
        response = {'status': 'success'}
        return jsonify(response)

# Utwórz nowy endpoint /employees/:id/subordinates metodą GET, który będzie zwracać wszystkich podwładnych danego pracownika. Do tego celu możesz użyć zapytania Cypher z wykorzystaniem relacji MANAGES.
def get_subordinates(tx, id):
    query = f"MATCH (m:Employee)-[:MANAGES]->(d:Department)<-[:WORKS_IN]-(e:Employee) WHERE id(m)={id} RETURN e"
    results = tx.run(query).data()
    subordinates = [{'name': result['e']['name'], 'position': result['e']['position']} for result in results]
    return subordinates

@app.route('/employees/<int:id>/subordinates', methods=['GET'])
def get_subordinates_route(id):
    with driver.session() as session:
        subordinates = session.read_transaction(get_subordinates, id)

    if not subordinates:
        response = {'error': 'Employee not found'}
        return jsonify(response), 404
    else:
        response = {'subordinates': subordinates}
        return jsonify(response)

# (10%) Dopisz obsługę zwrotu informacji o departamencie, w którym pracuje dany pracownik. Użytkownik powinien mieć możliwość pobrania nazwy departamentu oraz szczegółów dotyczących tego departamentu (np. liczby pracowników, menadżera).
def get_department(tx, id):
    query = f"MATCH (e:Employee)-[:WORKS_IN]->(d:Department) WHERE id(e)={id} RETURN d"
    result = tx.run(query).data()
    if not result:
        return None
    department = {'name': result[0]['d']['name']}
    query = f"MATCH (d:Department)<-[r:WORKS_IN]-(e:Employee) WHERE id(d)={result[0]['d'].id} RETURN e"
    employees = tx.run(query).data()
    department['employees'] = len(employees)
    if result[0]['d']['manager']:
        department['manager'] = result[0]['d']['manager']
    return department

@app.route('/employees/<int:id>/department', methods=['GET'])
def get_department_route(id):
    with driver.session() as session:
        department = session.read_transaction(get_department, id)

    if not department:
        response = {'error': 'Employee not found'}
        return jsonify(response), 404
    else:
        response = {'department': department}
        return jsonify(response)
    
# Napisz funkcję, która będzie zwracać wszystkie departamenty z bazy danych na endpoint /departments metodą GET. Użytkownik powinien mieć możliwość filtrowania i sortowania departamentów według różnych kryteriów (np. według nazwy, liczby pracowników).
def get_departments(tx, args=None):
    query = ""
    if args:
        query = resolve_sort_filter(args)
    else:
        query = "MATCH (d:Department) RETURN d"
    results = tx.run(query).data()
    departments = [{'name': result['d']['name']} for result in results]
    return departments

@app.route('/departments', methods=['GET'])
def get_departments_route():
    args = request.args
    with driver.session() as session:
        departments = session.read_transaction(get_departments, args)

    response = {'departments': departments}
    return jsonify(response)

# Utwórz nowy endpoint /departments/:id/employees metodą GET, który będzie zwracać wszystkich pracowników danego departamentu. Do tego celu możesz użyć zapytania Cypher z wykorzystaniem relacji WORKS_IN.
def get_employees_by_department(tx, id):
    query = f"MATCH (d:Department)<-[:WORKS_IN]-(e:Employee) WHERE id(d)={id} RETURN e"
    results = tx.run(query).data()
    employees = [{'name': result['e']['name'], 'position': result['e']['position']} for result in results]
    return employees

@app.route('/departments/<int:id>/employees', methods=['GET'])
def get_employees_by_department_route(id):
    with driver.session() as session:
        employees = session.read_transaction(get_employees_by_department, id)

    if not employees:
        response = {'error': 'Department not found'}
        return jsonify(response), 404
    else:
        response = {'employees': employees}
        return jsonify(response)
    

    

if __name__ == '__main__':
    app.run()