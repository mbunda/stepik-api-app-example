// Dodaj label Employee
CREATE (:Employee {name: 'Johdsn Dodse', position: 'Developer'})
CREATE (:Employee {name: 'Janeeee Smith', position: 'Manager'})
CREATE (:Employee {name: 'Bob Johnson', position: 'Designer'})

// Dodaj label Department
CREATE (:Department {name: 'IT'})
CREATE (:Department {name: 'HR'})
CREATE (:Department {name: 'Marketing'})

// Dodaj relacje WORKS_IN
MATCH (e:Employee {name: 'Johdsn Dodse'}), (d:Department {name: 'IT'})
CREATE (e)-[:WORKS_IN]->(d)

MATCH (e:Employee {name: 'Janeeee Smith'}), (d:Department {name: 'IT'})
CREATE (e)-[:WORKS_IN]->(d)

MATCH (e:Employee {name: 'Bob Johnson'}), (d:Department {name: 'Marketing'})
CREATE (e)-[:WORKS_IN]->(d)

// Dodaj relacje MANAGES
MATCH (m:Employee {name: 'Janeeee Smith'}), (d:Department {name: 'IT'})
CREATE (m)-[:MANAGES]->(d)

// Dodaj więcej pracowników
CREATE (:Employee {name: 'Alice Brown', position: 'Analyst'})
CREATE (:Employee {name: 'Charlie Wilson', position: 'Developer'})
CREATE (:Employee {name: 'Eva Garcia', position: 'HR Specialist'})

// Dodaj więcej departamentów
CREATE (:Department {name: 'Finance'})
CREATE (:Department {name: 'Operations'})
CREATE (:Department {name: 'Sales'})

// Dodaj relacje WORKS_IN
MATCH (e:Employee {name: 'Alice Brown'}), (d:Department {name: 'Finance'})
CREATE (e)-[:WORKS_IN]->(d)

MATCH (e:Employee {name: 'Charlie Wilson'}), (d:Department {name: 'Operations'})
CREATE (e)-[:WORKS_IN]->(d)

MATCH (e:Employee {name: 'Eva Garcia'}), (d:Department {name: 'HR'})
CREATE (e)-[:WORKS_IN]->(d)

// Dodaj relacje MANAGES
MATCH (m:Employee {name: 'Eva Garcia'}), (d:Department {name: 'HR'})
CREATE (m)-[:MANAGES]->(d)

// Dodaj dodatkowe relacje WORKS_IN
MATCH (e:Employee {name: 'Bob Johnson'}), (d:Department {name: 'Sales'})
CREATE (e)-[:WORKS_IN]->(d)

MATCH (e:Employee {name: 'Charlie Wilson'}), (d:Department {name: 'Sales'})
CREATE (e)-[:WORKS_IN]->(d)

// Dodaj więcej pracowników
CREATE (:Employee {name: 'Grace Miller', position: 'Marketing Specialist'})
CREATE (:Employee {name: 'Daniel White', position: 'Operations Manager'})
CREATE (:Employee {name: 'Olivia Taylor', position: 'Sales Representative'})

// Dodaj więcej departamentów
CREATE (:Department {name: 'Customer Service'})
CREATE (:Department {name: 'Research and Development'})
CREATE (:Department {name: 'Quality Assurance'})

// Dodaj relacje WORKS_IN
MATCH (e:Employee {name: 'Grace Miller'}), (d:Department {name: 'Sales'})
CREATE (e)-[:WORKS_IN]->(d)

MATCH (e:Employee {name: 'Daniel White'}), (d:Department {name: 'Research and Development'})
CREATE (e)-[:WORKS_IN]->(d)

MATCH (e:Employee {name: 'Olivia Taylor'}), (d:Department {name: 'Customer Service'})
CREATE (e)-[:WORKS_IN]->(d)

// Dodaj relacje MANAGES
MATCH (m:Employee {name: 'Daniel White'}), (d:Department {name: 'Research and Development'})
CREATE (m)-[:MANAGES]->(d)

// Dodaj dodatkowe relacje WORKS_IN
MATCH (e:Employee {name: 'Grace Miller'}), (d:Department {name: 'Customer Service'})
CREATE (e)-[:WORKS_IN]->(d)

MATCH (e:Employee {name: 'Olivia Taylor'}), (d:Department {name: 'Quality Assurance'})
CREATE (e)-[:WORKS_IN]->(d)

// Dodaj więcej pracowników
CREATE (:Employee {name: 'Samuel Turner', position: 'Quality Control Specialist'})
CREATE (:Employee {name: 'Sophia Lee', position: 'Customer Service Representative'})
CREATE (:Employee {name: 'Michael Rodriguez', position: 'Research Scientist'})

// Dodaj więcej departamentów
CREATE (:Department {name: 'Logistics'})
CREATE (:Department {name: 'Legal'})
CREATE (:Department {name: 'Production'})

// Dodaj relacje WORKS_IN
MATCH (e:Employee {name: 'Samuel Turner'}), (d:Department {name: 'Logistics'})
CREATE (e)-[:WORKS_IN]->(d)

MATCH (e:Employee {name: 'Sophia Lee'}), (d:Department {name: 'Customer Service'})
CREATE (e)-[:WORKS_IN]->(d)

MATCH (e:Employee {name: 'Michael Rodriguez'}), (d:Department {name: 'Production'})
CREATE (e)-[:WORKS_IN]->(d)

// Dodaj relacje MANAGES
MATCH (m:Employee {name: 'Michael Rodriguez'}), (d:Department {name: 'Production'})
CREATE (m)-[:MANAGES]->(d)

// Dodaj dodatkowe relacje WORKS_IN
MATCH (e:Employee {name: 'Samuel Turner'}), (d:Department {name: 'Legal'})
CREATE (e)-[:WORKS_IN]->(d)

MATCH (e:Employee {name: 'Sophia Lee'}), (d:Department {name: 'Logistics'})
CREATE (e)-[:WORKS_IN]->(d)
